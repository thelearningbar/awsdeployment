package com.example.lambda.demo;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class HelloPojo implements RequestHandler<RequestClass, Integer> 
{
	
	@Override
	public Integer handleRequest(RequestClass input, Context context) 
	{
		
		   context.getLogger().log("Input: "+input);
		   
	       try 
	       {
	    	   
	    	   	  Class.forName("com.mysql.jdbc.Driver");
	    	   	  String url1 = "jdbc:mysql://mysql-instance1.chhtkckrgigt.ca-central-1.rds.amazonaws.com:3306/mySQLDatabase";
	    	   	  String url2 ="jdbc:mysql://readdbinstance.chhtkckrgigt.ca-central-1.rds.amazonaws.com:3306/mySQLDatabase1";
	    	      String username1 = "gurminder08";
	    	      String username2 = "gurminder95";
	    	      String password1 = "Fall2017##";
	    	      Connection con1 = DriverManager.getConnection(url1, username1, password1);
	    	      Connection con2 = DriverManager.getConnection(url2,username2,password1);
	    	      Statement stmt1 = con1.createStatement();
	    	      Statement stmt2 = con2.createStatement();
//	    	      int val = stmt1.executeUpdate("insert into FIRSTTABLE values (1,10,'Max')");
	    	      ResultSet rs = stmt1.executeQuery("select * from FIRSTTABLE");
	    	      
	    	      while(rs.next())
	    	      {
	    	    	  
	    	    	  	  int myKey = rs.getInt("MyKey");
	    	    		  int id = rs.getInt("ID");
	        	    	  String content = rs.getString("Content");
	        	    	  stmt2.executeUpdate("insert into Persons "+"values("+id+","+myKey+",'"+content+"')");
//	    	    	 	  context.getLogger().log(id+", "+myKey+", "+content+" ");
	        	    	  
	    	       }
	    	      
	    	      context.getLogger().log("Successfully inserted entry into SECONDTABLE");
	    	      rs.close();
//	    	      Thread.sleep(5000);
//	    	      System.out.println("Time remaining in milliseconds: " + context.getRemainingTimeInMillis());
	    	  	
	       }
	       
	       catch(Exception e)
	       {
	    	   
		    	   e.printStackTrace();
		    	   context.getLogger().log("Caught Exception: "+e.getMessage());
		    	   
	       }
	       
	       
        
	       	return input.getNum1()+input.getNum2();
        
//          return 10;
	}
	
	
	
	public static String byteBufferToString(ByteBuffer buffer, Charset charset) 
    {
    	
        byte[] bytes;
        
        if (buffer.hasArray()) 
        {
            bytes = buffer.array();
        } 
        
        else 
        {
            bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
        }
        
        return new String(bytes, charset);
        
    }

}
