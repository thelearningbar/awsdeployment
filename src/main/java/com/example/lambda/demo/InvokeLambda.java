package com.example.lambda.demo;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;


import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;

public class InvokeLambda 
{
	
    private static final String awsAccessKeyId = "AKIAJ4AJCWBXCK4AZYJA";
    private static final String awsSecretAccessKey = "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";
    private static final String regionName = "ca-central-1";
    private static final String functionName = "MyInputOuputFunction";
    private static AWSCredentials credentials;
    private static AWSLambda lambdaClient;

    /**
     * The entry point into the AWS lambda function.
     */
    public static void main(String args[]) 
    {
        
    	
    	  credentials = new BasicAWSCredentials(awsAccessKeyId,awsSecretAccessKey);
    	
    	  lambdaClient = AWSLambdaClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(regionName).build();

        try 
        {
        	
        	int num1 =0;
        	int num2 =1;
        	String json = "{ \"num1\" : \" "+num1+ " \" , \"num2\" :  \" "+num2+" \"  }";
            InvokeRequest invokeRequest = new InvokeRequest();
          
            
            invokeRequest.setFunctionName(functionName);
            invokeRequest.setPayload(json);
            
            
//            for(int i=1;i<100;i++)
//            {
            	
	            System.out.println(byteBufferToString(
	            		lambdaClient.invoke(invokeRequest).getPayload(),
	                    Charset.forName("UTF-8")));
	            
//            }
            
        } 
        catch (Exception e) 
        {
        	
            //logger.error(e.getMessage());
             System.out.println(e.getMessage());

        }
        
    }

    public static String byteBufferToString(ByteBuffer buffer, Charset charset) 
    {
    	
        byte[] bytes;
        
        if (buffer.hasArray()) 
        {
            bytes = buffer.array();
        } 
        
        else 
        {
            bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
        }
        
        return new String(bytes, charset);
        
    }
	
}
